exception Cycle
type mark = NotVisited | InProgress | Visited

type 'a graph =
    { mutable g_nodes : 'a node list }
and 'a node = {
  n_label : 'a;
  mutable n_mark : mark;
  mutable n_link_to : 'a node list;
  mutable n_linked_by : 'a node list;
}

let mk_graph () = { g_nodes = [] }

let add_node g x =
  let n = { n_label = x; n_mark = NotVisited; n_link_to = []; n_linked_by = [] } in
  g.g_nodes <- n :: g.g_nodes

let node_of_label g x =
  List.find (fun n -> n.n_label = x) g.g_nodes

let add_edge g id1 id2 =
  try
    let n1 = node_of_label g id1 in
    let n2 = node_of_label g id2 in
    n1.n_link_to   <- n2 :: n1.n_link_to;
    n2.n_linked_by <- n1 :: n2.n_linked_by
  with Not_found -> Format.eprintf "Tried to add an edge between non-existing nodes"; raise Not_found

(* ici le graph est une liste de noeud, chacune contenant une liste de noeuds avec lequel il est lié*)

let clear_marks g =
  List.iter (fun n -> n.n_mark <- NotVisited) g.g_nodes


let find_roots g =
  List.filter (fun n -> n.n_linked_by = []) g

let rec is_in_cycle n g =
  if n.n_mark = InProgress then
    (
    clear_marks g;
     true)
  else
    (n.n_mark <- InProgress;
    if n.n_link_to = [] then
      (n.n_mark  <- Visited;
     clear_marks g;
       false)
    else
        List.exists ( fun m -> is_in_cycle m g) n.n_link_to)

let has_cycle g =
  List.exists ( fun n -> is_in_cycle n g) g.g_nodes


    (* 
    clear_marks g;if a = true then true
    if a = false then false *)

let all_not_in l1 l2 =
  List.filter(fun a -> List.for_all(fun b -> a!=b) l2) l1



let update_node n l2 =
  n.n_linked_by <- all_not_in n.n_linked_by l2;
  n

  let eliminate l1 l2 =
    List.map(fun n -> update_node n l2) (all_not_in l1 l2)

let rec trier l1 =
    match find_roots l1 with
      | [] -> l1
      | l2 -> List.append l2 (trier ( eliminate l1 l2))

  (*  match l with
    | [] -> g
    | _ -> List.append l (trier g.g_nodes);;*)

    

let topological1 g =
  if has_cycle g then raise Cycle 
  else trier g.g_nodes

let topological g =
  List.map(fun n -> n.n_label ) (topological1 g)

let topological2 g =
  g.g_nodes <- topological1 g;
  g


