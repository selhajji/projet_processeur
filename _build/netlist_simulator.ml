open Netlist_ast

let print_only = ref false
let number_steps = ref (-1)

(* Définition RAM & ROM*)

let ram = Array.make 4096 false

let rom = Array.make 1024 false

(* Fonctions utilitaires *)
let bool_of_int = function
  | 0 -> VBit false
  | 1 -> VBit true

let int_of_bool = function
  | VBit false -> 0
  | VBit true -> 1

let int_of_bool_2 = function
  | false -> 0
  | true -> 1

let rec pow x n = match n with
  | 0 -> 1
  | 1 -> x 
  | _ -> x*pow x (n-1)

let test_arr = function 
  | VBit a -> false
  | VBitArray a -> true

let rec boulean_to_int x p =
  match p with
    |0 -> int_of_bool_2 (Array.get x p)
    |_ -> (int_of_bool_2 (Array.get x p))*(pow 2 p)
    +boulean_to_int x (p-1)

let get_b = function
  | VBit b -> b
  | _ -> false

let get_a = function
  | VBitArray b -> b
  | _ -> Array.make 1 false

let get_var arg var =
  match arg with
  | Aconst value -> get_b value
  | Avar ident -> get_b (Env.find ident var)

let get_arr arg var =
  match arg with
  | Aconst value -> get_a value
  | Avar ident -> get_a (Env.find ident var)

(* read/write in memory :  *)
    
let read_ROM addr_size word_size addr =
  let position = boulean_to_int addr addr_size in
    let read = Array.make word_size false
  in let () =
    for p = 0 to word_size-1 do
      Array.set read p (Array.get rom (position+p))
    done
    in match word_size with
      | 1 -> VBit (Array.get read 0)
      | _ -> VBitArray read

let write_RAM addr_size write_addr write_data word_size =
  let position = boulean_to_int write_addr addr_size in
  let () =
  for p = 0 to word_size-1 do
    Array.set ram (position+p) (Array.get write_data p)
  done in ()

let rw_RAM addr_size word_size read_addr 
  write_enable write_addr write_data =
  let () = if write_enable then
    write_RAM addr_size write_addr write_data word_size
  in let position = boulean_to_int read_addr addr_size in
    let read = Array.make word_size false
  in let () =
    for p = 0 to word_size-1 do
      Array.set read p (Array.get ram (position+p))
    done
    in match word_size with
      | 1 -> VBit (Array.get read 0)
      | _ -> VBitArray read

(* gestion des I/O*)

let rec ask id = 
  print_string (id ^ "=? \n");
  let a= read_int() in
  if (a=0) || (a=1) then bool_of_int a
  else begin
    Format.eprintf "not a binary input.@.";
    ask id
  end

let show outputs var =
  List.iter (fun key ->
    (*let () = Hashtbl.add reg key value in *)
    let value = Env.find key var in 
    let digit = int_of_bool value in
    let str= key ^ " = " ^ string_of_int digit ^ "\n" in
    print_string str
    ) outputs

let rec get_input mapping keys =
  match keys with
  | [] -> mapping
  | x::r -> get_input (Env.add x (ask x) mapping) r


(* opérateurs*)

let bin_op op arg1  arg2 = match op with
  | Or -> arg1 || arg2
  | Xor -> (arg1 || arg2) && (not (arg1 && arg2))
  | And -> arg1 && arg2
  | Nand -> not (arg1 && arg2)

let mux arg1 arg2 arg3 = match arg1 with
    | false -> arg2
    | true -> arg3

(* usage des registres*)
let rgst reg key =
  try Hashtbl.find reg key
  with Not_found ->
    let a = VBit false in 
    let () = Hashtbl.add reg key a in 
    VBit false

(* opération(s) sur les bus*)

let slice a1 a2 arg =
  let n = a2-a1 in 
  let read = Array.make n false
in let () =
  for p = 0 to n do
    Array.set read p (Array.get arg (a1+n))
  done
  in match n with
    | 1 -> VBit (Array.get read 0)
    | _ -> VBitArray read

let expr exp var reg =
  (* raccourcis pour accéder à à la valeur (v)
     ou au tableau (a) que désigne un argument
     en utilisant directement get_var et get_arr cela
     rendrait *)
  let v = fun arg -> get_var arg var in
  let a = fun arg -> get_arr arg var in
  match exp with
  | Earg arg ->  VBit (v arg)
  | Ereg ident -> rgst reg ident
  | Enot arg -> VBit (not (v arg))
  | Ebinop (op, arg1,  arg2) -> VBit (bin_op op (v arg1) (v arg2))
  | Emux (arg1, arg2, arg3) -> VBit (mux (v arg1) (v arg2) (v arg3))
  | Erom (a1,a2,arg) -> read_ROM a1 a2 (a arg) 
  | Eram (a1,a2,arg1,arg2,arg3,arg4) -> rw_RAM a1 a2 (a arg1) (v arg2) (a arg3) (a arg4)
  (* note : on voit que l'adresse doit être au moins sur 2 bits*)  
  | Econcat (arg1,arg2) -> VBitArray (Array.append (a arg1) (a arg2))
  | Eslice (a1,a2,arg) -> slice a1 a2 (a arg)
  | Eselect (n,arg) -> VBit (Array.get (a arg) n)


let solve (ident, exp) var reg =
  let a= expr exp var reg in
  Env.add ident a var


let rec solving var eqs reg =
    match eqs with
    | [] -> var
    | eq::r -> solving (solve eq var reg) r reg


let solver p reg =
  let var = get_input Env.empty p.p_inputs in
  let eqs = p.p_eqs in
  solving var eqs reg

let rec from_out l env =
  match l with 
  | [] -> env
  | k::tl -> from_out tl (Env.add k (VBit false) env)

let update_reg reg var =
  Env.iter (fun key value ->
    if Hashtbl.mem reg key then
      Hashtbl.replace reg key value 
    ) var

(* exécutino*)
let simulator p n = (*program number_steps*)
(* initialisation des registres :*)
  let reg =  Hashtbl.create 12345 in 
  for i = 1 to n do
    print_string ("step n°" ^ string_of_int i ^ "\n");
   (* let inputs = get_input Env.empty p.p_inputs in
    let outputs = from_out p.p_outputs Env.empty in *)
    let var = solver p reg in
    let () = update_reg reg var in
    show p.p_outputs var ;

  done


let compile filename =
  try
    let p = Netlist.read_file filename in
    begin try
        let p = Scheduler.schedule p in
        simulator p !number_steps
      with
        | Scheduler.Combinational_cycle ->
            Format.eprintf "The netlist has a combinatory cycle.@.";
    end;
  with
    | Netlist.Parse_error s -> Format.eprintf "An error accurred: %s@." s; exit 2

let main () =
  Arg.parse
    ["-n", Arg.Set_int number_steps, "Number of steps to simulate"]
    compile
    ""
;;

main ()

