open Netlist_ast
open Graph

exception Combinational_cycle

let filter_reg eq = match snd eq with
  | Ereg ident-> true
  | Eram (a,b,arg1,arg2,arg3,arg4) -> true
  | _ -> false


let read_eq_only exp = match exp with
  | Earg arg -> [arg]
  | Ereg ident -> []
  | Enot arg -> [arg]
  | Ebinop (op, arg1,  arg2) -> [arg1;arg2]
  | Emux (arg1, arg2, arg3) -> [arg1;arg2;arg3]
  | Erom (a,b,arg) -> [arg]
  | Eram (a,b,arg1,arg2,arg3,arg4) -> [arg1;arg2;arg3;arg4]
  | Econcat (arg1,arg2) -> [arg1;arg2]
  | Eslice (a,b,arg) -> [arg]
  | Eselect (a,arg) -> [arg]


let only_in_one l1 l2 =
  let l3 = List.filter(fun a -> List.for_all(fun b -> a!=b) l2) l1  in
  let l4 = List.filter(fun a -> List.for_all(fun b -> a!=b) l1) l2  in
  l3 @ l4

let filter_arg arg = match arg with
  | Avar ident -> true
  | Aconst value -> false

let arg_to_ident input = match input with
  | Avar ident -> ident
  | _ -> "default"

let read_exp eq = (* failwith "Scheduler.read_exp: Non implementé" *)
  let exp = snd eq in 
  let inputs = read_eq_only exp in
  let inputs2 = List.filter filter_arg inputs in
  List.map arg_to_ident inputs2


let filtre inputs all_outputs =
  List.filter( fun x -> List.exists(fun y -> x=y) all_outputs) inputs

let indice var all_outputs nodes =
  let m = List.find(fun n -> List.nth all_outputs n.n_label = var) nodes in 
  m.n_label

let add_eq_in_g eq g all_outputs=
  let inputs = filtre (read_exp eq) all_outputs in
  let inputs2 = List.map( fun el -> indice el all_outputs g.g_nodes ) inputs in
  let out = indice (fst eq ) all_outputs g.g_nodes in
  List.iter (fun n -> add_edge g n out) inputs2



(*let form_graph p =
peq
  g *)
let schedule p =
  let g = mk_graph () in
  let (eq_reg,eq_not_req) = List.partition filter_reg p.p_eqs in 
  let outputs = List.map fst eq_not_req in
  List.iteri (fun i x -> add_node g i ) eq_not_req;
  List.iter (fun eq -> add_eq_in_g eq g outputs) eq_not_req;
  begin try
    let h = topological g in 
    let p_eqs = List.map(fun n -> List.nth eq_not_req n ) h in
    { p_eqs = eq_reg @ p_eqs; 
    p_inputs = p.p_inputs;
    p_outputs = p.p_outputs;
    p_vars = p.p_vars };
  with 
    | Graph.Cycle -> raise Combinational_cycle
    | e -> Format.eprintf "other problem"; raise e
  end

let schedule_old p =
  let g = mk_graph () in
  let outputs = List.map fst p.p_eqs in
  List.iteri (fun i x -> add_node g i ) p.p_eqs;
  List.iter (fun eq -> add_eq_in_g eq g outputs) p.p_eqs;
  begin try
    let h = topological g in 
    let p_eqs = List.map(fun n -> List.nth p.p_eqs n ) h in
    { p_eqs = p_eqs; 
    p_inputs = p.p_inputs;
    p_outputs = p.p_outputs;
    p_vars = p.p_vars };
  with 
    | Graph.Cycle -> raise Combinational_cycle
    | e -> Format.eprintf "other problem"; raise e
  end

(*
let schedule2 p =
  let g = mk_graph () in
  let outputs = only_in_one (List.map fst p.p_eqs)  p.p_outputs in
(*  List.iteri (fun i x -> add_node g i ) (p.p_inputs @ outputs); *)
  let l = p.p_inputs @ outputs
  List.iteri (fun i x -> add_node g i ) l;
  (*let l = List.map(List.find p.p_eqs) g.g_nodes *)
  List.iter (fun eq -> add_eq_in_g eq g p.p_eqs) p.p_eqs;
  begin try
    let h = topological g in 
    p.p_peqs <- List.map(fun x -> List.find(fun y -> x = fst y) p.p_peqs) h;
    p;
  with 
    | Graph.Cycle -> raise Combinational_cycle
    | e -> Format.eprintf "other problem"; raise e
  end
  *)